//
//  Vector.swift
//  AlienGame
//
//  Created by Bartosz Szewczyk on 31.10.2015.
//  Copyright © 2015 Bartosz Szewczyk. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

func vecAdd(a:CGPoint, b:CGPoint)->CGPoint
{
    
    return CGPointMake(a.x + b.x, a.y + b.y);
}
func vecSub(a:CGPoint, b:CGPoint)->CGPoint
{
    return CGPointMake(a.x - b.x, a.y-b.y);
}
func vecMult(a:CGPoint,b:CGFloat)->CGPoint
{
    return CGPointMake(a.x * b, a.y * b);
}
func vecLength(a:CGPoint)->CGFloat
{
    return CGFloat(sqrt(CFloat(a.x) * CFloat(a.x) + CFloat(a.y) * CFloat(a.y)))
}
func vecNormalize(a:CGPoint)->CGPoint
{
    var length:CGFloat = vecLength(a);
    return CGPointMake(a.x / length, a.y / length);
    
}

