//
//  GameViewController.swift
//  AlienGame
//
//  Created by Bartosz Szewczyk on 29.10.2015.
//  Copyright (c) 2015 Bartosz Szewczyk. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class GameViewController: UIViewController {
    
    var backgroundMusicPlayer:AVAudioPlayer = AVAudioPlayer();
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    override func viewWillLayoutSubviews() {

        let bgMusicURL:NSURL = NSBundle.mainBundle().URLForResource("bgmusic", withExtension: "mp3")!;
        
        try! backgroundMusicPlayer = AVAudioPlayer(contentsOfURL: bgMusicURL)
        
        backgroundMusicPlayer.numberOfLoops = -1;
        backgroundMusicPlayer.prepareToPlay();
        backgroundMusicPlayer.play();
        
        let skView:SKView = self.view as! SKView;
        
        skView.showsFPS = true;
        //skView.showsPhysics = true;
        skView.showsNodeCount = true;
        
        let scene:SKScene = GameScene(size:skView.bounds.size)
        scene.scaleMode = SKSceneScaleMode.AspectFill;
        skView.presentScene(scene);

    }
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
