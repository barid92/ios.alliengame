//
//  GameScene.swift
//  AlienGame
//
//  Created by Bartosz Szewczyk on 29.10.2015.
//  Copyright (c) 2015 Bartosz Szewczyk. All rights reserved.
//

import SpriteKit
import GameplayKit
class GameScene: SKScene , SKPhysicsContactDelegate{
    
    var player:SKSpriteNode = SKSpriteNode()
    var lastYieldTimeInterval:NSTimeInterval = NSTimeInterval();
    var lastUpdateTimeInterval:NSTimeInterval = NSTimeInterval();
    var aliensDestroyed:Int = 0;
    
    let alienCategory:UInt32 = 0x1 << 1;
    let photonTorpedoCategory:UInt32 = 0x1 << 0;
    
    override func didMoveToView(view: SKView)
    {
        self.physicsWorld.contactDelegate = self;
    }
    
    override init(size:CGSize)
    {
        super.init(size: size)
        self.backgroundColor = SKColor.blackColor();
        player = SKSpriteNode(imageNamed: "shuttle");
        player.position = CGPointMake(self.frame.size.width/2, player.size.height/2 + 20);
        addAlien();
        self.addChild(player);
        self.physicsWorld.gravity = CGVectorMake(0, 0);
        self.physicsWorld.contactDelegate = self;
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addAlien()
    {
        var alien:SKSpriteNode = SKSpriteNode();
        alien = SKSpriteNode(imageNamed: "alien");
        alien.physicsBody = SKPhysicsBody(rectangleOfSize: alien.size);
        alien.physicsBody?.dynamic = true;
        alien.physicsBody?.categoryBitMask = alienCategory;
        alien.physicsBody?.contactTestBitMask = photonTorpedoCategory;
        alien.physicsBody?.collisionBitMask = 0;
        
        let minX = alien.size.width/2;
        let maxX = self.frame.size.width - alien.size.width/2;
        let rageX = maxX - minX;
        let position:CGFloat = (CGFloat(arc4random()) % CGFloat(rageX) + CGFloat(minX));
        
        alien.position = CGPointMake(position, self.frame.size.height + alien.size.height)
        
        self.addChild(alien);
        
        let minDuration = 2;
        let maxDuration = 4;
        
        let rangeDuration = maxDuration - minDuration;
    
        let duration = GKRandomSource.sharedRandom().nextIntWithUpperBound(rangeDuration) + minDuration;
        
        
        var actionArray = [SKAction]();
        
        actionArray.append(SKAction.moveTo(CGPointMake(position, -alien.size.height), duration: NSTimeInterval(duration)))
        
        actionArray.append(SKAction.removeFromParent());
        
        alien.runAction(SKAction.sequence(actionArray));
        
        
    }
        func updateWithTimeSinceLastUpdate(timeSinceLastUpdate:CFTimeInterval)
    {
        lastYieldTimeInterval += timeSinceLastUpdate;
        if(lastYieldTimeInterval > 1)
        {
            lastYieldTimeInterval = 0;
            addAlien();
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
    }
   
        override func update(currentTime: CFTimeInterval) {
        var timeSinceLastUpdate = currentTime - lastUpdateTimeInterval;
        lastUpdateTimeInterval = currentTime;
        
        if(timeSinceLastUpdate > 1)
        {
            timeSinceLastUpdate = 1/60;
            lastUpdateTimeInterval = currentTime;
        }
        updateWithTimeSinceLastUpdate(timeSinceLastUpdate);
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        CreateTorpedo(touches);
    }
    func CreateTorpedo(touches: Set<UITouch>)
    {
        self.runAction(SKAction.playSoundFileNamed("torpedo.mp3", waitForCompletion: false));
        let touch:UITouch? = (touches.first! as UITouch);
        let location:CGPoint? = touch?.locationInNode(self)
        
        let torpedo:SKSpriteNode = SKSpriteNode(imageNamed: "torpedo");
        torpedo.position = player.position;
        torpedo.physicsBody = SKPhysicsBody(circleOfRadius: torpedo.size.width / 2);
        torpedo.physicsBody?.dynamic = true;
        torpedo.physicsBody?.categoryBitMask = photonTorpedoCategory;
        torpedo.physicsBody?.contactTestBitMask = alienCategory;
        torpedo.physicsBody?.collisionBitMask = 0;
        torpedo.physicsBody?.usesPreciseCollisionDetection = true;
        
        let offset:CGPoint = vecSub(location!, b: torpedo.position)
        
        if(offset.y < 0)
        {
            return;
        }
        self.addChild(torpedo);
        
        let direction:CGPoint = vecNormalize(offset);
        
        let shoutLength:CGPoint = vecMult(direction, b: 1000);
        
        let finalDestination:CGPoint = vecAdd(shoutLength, b: torpedo.position);
        
        let velocity = 568/1;
        let moveDuration:Float = Float(self.size.width)/Float(velocity);
        
        var actionArray = [SKAction]();
        
        actionArray.append(SKAction.moveTo(finalDestination, duration: NSTimeInterval(moveDuration)));
        actionArray.append(SKAction.removeFromParent());
        
        torpedo.runAction(SKAction.sequence(actionArray));
    }

    
       func didBeginContact(contact: SKPhysicsContact) {
        var firstBody:SKPhysicsBody;
        var secondBody:SKPhysicsBody;
        if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
        {
            firstBody = contact.bodyA;
            secondBody = contact.bodyB;
        }
        else
        {
            firstBody = contact.bodyB;
            secondBody = contact.bodyA;
        }

        
        if((firstBody.categoryBitMask & photonTorpedoCategory != 0) && (secondBody.categoryBitMask & alienCategory != 0))
        {
                    TorpedoDidCollideWithAlien(firstBody.node as! SKSpriteNode, Alien: secondBody.node as! SKSpriteNode);
        }
    }
    func TorpedoDidCollideWithAlien(Torpedo:SKSpriteNode, Alien:SKSpriteNode)
    {
        print("HIT!");
        Torpedo.removeFromParent();
        Alien.removeFromParent();
        aliensDestroyed++;
        if(aliensDestroyed>10)
        {
            //Game Win
        }
    }
}
